﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectAle
{
    class Player
    {
        public string name;
        public int score;
        public int posision;

        public Player()
        {
            name = "BauBau";
            score = 0;
            posision = 0;
        }

        public Player(string name, int score)
        {
            this.name = name;
            this.score = score;
            posision = 0;
        }

    }
        
}
    